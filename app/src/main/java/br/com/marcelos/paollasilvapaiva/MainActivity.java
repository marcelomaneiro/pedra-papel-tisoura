package br.com.marcelos.paollasilvapaiva;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import br.com.marcelos.paollasilvapaiva.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }


    public void selecionarPedra(View view){
        this.opcaoSelecionada("pedra");
    }
    public void selecionarPapel(View view){
        this.opcaoSelecionada("papel");
    }
    public void selecionarTesoura(View view){
        this.opcaoSelecionada("tesoura");
    }
    public void opcaoSelecionada(String escolhaUsuario){

        ImageView imagemResultado = findViewById(R.id.imageResultado);
        ImageView imagePedra = findViewById(R.id.imagePedra);
        ImageView imagePapel = findViewById(R.id.imagePapel);
        ImageView imageTesoura = findViewById(R.id.imageTesoura);

        TextView textoResultado = findViewById(R.id.textResultado);
        TextView jogador = findViewById(R.id.textJogador);
        TextView oponente = findViewById(R.id.textOponente);

        String[] opcoes = {"pedra","papel","tesoura"};
        int numero = new Random().nextInt(3);
        String escolhaApp = opcoes[numero];

        switch (escolhaApp){
            case "pedra": imagemResultado.setImageResource(R.drawable.pedra);
                imagePedra.setBackgroundColor(Color.blue(20));
                break;
            case "papel": imagemResultado.setImageResource(R.drawable.papel);
                imagePapel.setBackgroundColor(Color.blue(20));
                break;
            case "tesoura": imagemResultado.setImageResource(R.drawable.tesoura);
                imageTesoura.setBackgroundColor(Color.blue(20));
                break;
        }

        int totalJogador = 0;
        int totalOponente = 0;
        // App é o ganhador
        if(
                (escolhaApp == "pedra" && escolhaUsuario == "tesoura") ||
                        (escolhaApp == "papel" && escolhaUsuario == "pedra") ||
                        (escolhaApp == "tesoura" && escolhaUsuario == "papel")
        ){
            totalOponente++;
            textoResultado.setText("Você perdeu :( ");
            //oponente.setText(textoResultado.getText());
        }else if(
                (escolhaUsuario == "pedra" && escolhaApp == "tesoura") ||
                        (escolhaUsuario == "papel" && escolhaApp == "pedra") ||
                        (escolhaUsuario == "tesoura" && escolhaApp == "papel")
        ){
            totalJogador++;
            textoResultado.setText("Parabéns!! Você foi o ganhador ;)");
            //jogador.setText(textoResultado.getText());
        }else {
            textoResultado.setText("Empatou!!!");
        }

    }
}
